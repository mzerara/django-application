# Dependencies
import os
import logging
import importlib.util


# Environment variables:
djangoProjectName = os.environ["DJANGO_PROJECT_NAME"]
sourceDirectoryName = os.environ["SOURCE_DIRECTORY_NAME"]


# Creating directories:
directories = [
    sourceDirectoryName
]
for directory in directories:
    if not os.path.isdir(sourceDirectoryName):
        logging.warning(
            "\n********** Creating '{}' directory **********\n".format(directory))
        os.mkdir(directory)


# List of commands:
commands = [
    {
        "task": "install_django",
        "message": "Installing Django and its dependencies",
        "cmd": "pipenv install {0} {1} {2} {3} {4}".format(
            "django==2.2.9",
            "djangorestframework",
            "django-cors-headers",
            "djangorestframework-simplejwt",
            "mysqlclient"
        )
    },
    {
        "task": "create_django_project",
        "message": "Creating a new Django project",
        "cmd": "pipenv run django-admin startproject {} ./{}".format(djangoProjectName, sourceDirectoryName)
    },
    {
        "task": "run_django_server",
        "message": "Running Django development server",
        "cmd": "pipenv run python ./{}/manage.py runserver 0.0.0.0:8000".format(sourceDirectoryName)
    }
]

# Execution:
for command in commands:

    # Set default command execution to true:
    executeCommand = True

    # List of tasks to execute:
    if command["task"] == "install_django" and os.system("pipenv run django-admin version") == 0:
        executeCommand = False

    if command["task"] == "create_django_project" and os.path.isdir("./{}/{}".format(sourceDirectoryName, djangoProjectName)):
        executeCommand = False

    # Execute command if needed:
    if executeCommand:
        logging.warning(
            "\n********** {} **********\n".format(command["message"]))
        os.system(command["cmd"])
