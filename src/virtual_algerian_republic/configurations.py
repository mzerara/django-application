configurations = {

    "models": {

        "content_app": {

            "media": {
                "podcast": "podcast",
                "reading": "Reading",
                "video": "video"
            },

            "categories": {
                "agriculture": "agriculture",
                "culture": "culture",
                "education": "education",
                "energy": "energy",
                "environnement": "environnement",
                "finance": "finance",
                "health": "health",
                "industry": "industry",
                "justice": "justice",
                "media": "media",
                "sport": "sport",
                "technology": "technology",
                "tourism": "tourism",
                "urbanism": "urbanism"
            },

            "status": {
                "active": "active",
                "inactive": "inactive"
            },

        },

        "contact_app": {

            "subjects": {
                "seek_information": "I can't find the information I seek",
                "create_account": "I cannot create my account",
                "read_content": "I cannot read the content",
                "suggest_ideas": "I have some suggestions to make",
                "join_team": "I wish to join the team",
                "others": "others"
            },

            "status": {
                "active": "active",
                "inactive": "inactive"
            },

            "response_status": {
                "pending": "pending",
                "answered": "answered"
            }

        }

    },

    "urls": {

        "content_app": {

            "list_of_categories": {
                "name": "category-list",
                "route": "category-list"
            }

        },

        "contact_app": {

            "list_of_subjects": {
                "name": "subject-list",
                "route": "subject-list"
            },

            "email": {
                "name": "email",
                "route": "email"
            }

        }

    },

    "error_messages": {

        "invalid_header_found": "Invalid header found!"

    }

}
