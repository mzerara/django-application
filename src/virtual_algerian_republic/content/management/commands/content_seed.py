# Dependencies
from django.core.management.base import BaseCommand
from django.conf import settings
from ...models import (
    Medium,
    Category
)


# Globals
content_model_configs = settings.PROJECT_CONFIGS["models"]["content_app"]


# Create your custom actions here.
class Command(BaseCommand):

    help = "This command will insert some initial content data in the database"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):

        # Ceate initial media elements
        self.stdout.write(
            "***** Inserting data into Medium table *****",
            ending='\n'
        )
        for medium in content_model_configs["media"].keys():
            Medium.objects.update_or_create(
                label=medium,
                description="this is the {} medium".format(medium)
            )

        # Create some categories
        self.stdout.write(
            "***** Inserting data into Category table *****",
            ending='\n'
        )
        for category in content_model_configs["categories"].keys():

            try:
                Category.objects.filter(
                    status="active"
                ).get(pk=category)

                self.stdout.write(
                    "***** the {} category exists already! *****".format(
                        category
                    ),
                    ending='\n'
                )

            except Category.DoesNotExist:
                category = Category.objects.create(
                    label=category,
                    description="this is the {} category".format(category)
                )
                category.media.set(
                    [
                        medium for medium in content_model_configs["media"].keys()
                    ]
                )
