# Dependencies
from django.contrib import admin
from .models import(
    Medium,
    Category
)


# Register models here
admin.site.register(Medium)
admin.site.register(Category)
