# Dependencies
from django.conf import settings
from django.urls import path
from .views import (
    CategoryListView
)


# Globals
content_url_configs = settings.PROJECT_CONFIGS["urls"]["content_app"]


# Register the application API urls
urlpatterns = [
    path(
        route=content_url_configs["list_of_categories"]["route"],
        name=content_url_configs["list_of_categories"]["name"],
        view=CategoryListView.as_view()
    )
]
