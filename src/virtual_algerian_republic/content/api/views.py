# Dependencies
from django.conf import settings
from rest_framework import (
    generics,
    mixins
)
from .serializers import (
    CategorySerializer
)
from ..models import (
    Category
)


# Globals
content_model_configs = settings.PROJECT_CONFIGS["models"]["content_app"]


# Category list API view
class CategoryListView(
    generics.GenericAPIView,
    mixins.ListModelMixin
):

    lookup_field = "pk"
    serializer_class = CategorySerializer
    queryset = Category.objects.all().filter(
        status=content_model_configs["status"]["active"]
    ).order_by('label')

    def get(self, request):
        return self.list(request)
