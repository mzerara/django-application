# Dependencies
from django.conf import settings
from django.urls import reverse, resolve
from django.test import SimpleTestCase
from rest_framework import status
from rest_framework.test import APITestCase
from .views import (
    CategoryListView
)
from ..models import (
    Medium,
    Category
)


# Globals
content_url_configs = settings.PROJECT_CONFIGS["urls"]["content_app"]


# Test suites
class ContentAPITestCase(APITestCase):

    def test_get_category_list(self):

        reversed_url = reverse(
            content_url_configs["list_of_categories"]["route"]
        )
        response = self.client.get(
            reversed_url,
            format="json"
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK
        )


class ContentUrlTestCase(SimpleTestCase):

    def test_category_list_url_is_resolved(self):
        reversed_url = reverse(
            content_url_configs["list_of_categories"]["route"]
        )
        resolved_url = resolve(reversed_url)
        self.assertEqual(resolved_url.func.view_class, CategoryListView)
