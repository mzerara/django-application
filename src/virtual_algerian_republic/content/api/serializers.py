# Dependencies
from rest_framework import serializers
from ..models import (
    Category,
)


# Category serializer
class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = (  # "__all__"
            "label",
            "media",
            "description",
            "created_at",
            "updated_at",
            "deleted_at"
            # "status"
        )
