# Dependencies
from django.db import models
from django.conf import settings


# Globals
content_model_configs = settings.PROJECT_CONFIGS["models"]["content_app"]


# Tag table
class Tag (models.Model):

    label = "TBD"

    description = models.TextField(
        blank=True
    )
    created_at = models.DateTimeField(
        auto_now=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        auto_now_add=False
    )
    deleted_at = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True
    )
    status = models.CharField(
        max_length=10,
        default=content_model_configs["status"]["active"],
        choices=[
            (key, value.capitalize()) for (key, value) in content_model_configs["status"].items()
        ]
    )

    def __str__(self):
        return self.label


# Medium table
class Medium (models.Model):

    label = models.CharField(
        primary_key=True,
        max_length=10,
        choices=[
            (key, value.capitalize()) for (key, value) in content_model_configs["media"].items()
        ]
    )
    description = models.TextField(
        blank=True
    )
    created_at = models.DateTimeField(
        auto_now=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        auto_now_add=False
    )
    deleted_at = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True
    )
    status = models.CharField(
        max_length=10,
        default=content_model_configs["status"]["active"],
        choices=[
            (key, value.capitalize()) for (key, value) in content_model_configs["status"].items()
        ]
    )

    def __str__(self):
        return self.label


# Category table
class Category(models.Model):

    label = models.CharField(
        primary_key=True,
        max_length=20,
        null=False,
        blank=False,
        choices=[
            (key, value.capitalize()) for (key, value) in content_model_configs["categories"].items()
        ]
    )
    media = models.ManyToManyField(
        to="Medium",
        blank=False
    )
    description = models.TextField(
        blank=True
    )
    created_at = models.DateTimeField(
        auto_now=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        auto_now_add=False
    )
    deleted_at = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True
    )
    status = models.CharField(
        max_length=10,
        default=content_model_configs["status"]["active"],
        choices=[
            (key, value.capitalize()) for (key, value) in content_model_configs["status"].items()
        ]
    )

    def __str__(self):
        return self.label
