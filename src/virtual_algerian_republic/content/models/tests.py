# Dependencies
from django.test import TestCase
from django.db import IntegrityError
from .models import (
    Tag,
    Medium,
    Category
)


# Test suites
class TagModelTest(TestCase):

    def test_tag_creation(self):
        pass


class MediumModelTest(TestCase):

    def test_medium_creation(self):

        medium = Medium.objects.create(
            label="podcast",
            description="this is a test description"
        )

        self.assertEqual(Medium.objects.count(), 1)
        self.assertTrue(isinstance(medium, Medium))
        self.assertEqual(medium.__str__(), medium.label)


class CategoryModelTest(TestCase):

    def test_category_creation_without_existing_medium(self):

        with self.assertRaises(IntegrityError):
            category = Category.objects.create(
                label="agriculture",
                description="this is a simple test"
            )
            category.media.set(["podcast"])
            self.assertEqual(Category.objects.count(), 0)

    def test_category_creation_with_existing_medium(self):

        medium_label = "podcast"

        Medium.objects.create(
            label=medium_label,
            description="this is a test description"
        )

        category = Category.objects.create(
            label="agriculture",
            description="this is a simple test"
        )
        category.media.set([medium_label])

        self.assertEqual(Category.objects.count(), 1)
        self.assertTrue(isinstance(category, Category))
        self.assertEqual(category.status, "active")
        self.assertEqual(category.__str__(), category.label)
