# Dependencies
import os
from .configurations import configurations


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Setup project configurations
PROJECT_CONFIGS = configurations


# Secret key
SECRET_KEY = os.environ["DJANGO_SECRET_KEY"]


# My project URL definition(s)
ROOT_URLCONF = "virtual_algerian_republic.urls"


# Static files (CSS, JavaScript, Images)
STATIC_URL = "/static/"


# Django CORS configs and headers
CORS_ORIGIN_WHITELIST = [
    os.environ["CORS_ORIGIN_WHITELIST"]
]
CORS_ALLOW_HEADERS = [
    "accept",
    "accept-encoding",
    "authorization",
    "content-type",
    "dnt",
    "origin",
    "user-agent",
    "x-csrftoken",
    "x-requested-with",
    "x-custom-header"
]


# Django rest framework configurations
REST_FRAMEWORK = {
    # Add configs later
}


# Development mode configs include:
# 1- Debug mode,
# 2- Allowed hosts,
# 3- Development server,
# 4- SMTP configs
if os.environ["ENV_MODE"] == "DEVELOPMENT":

    DEBUG = True

    ALLOWED_HOSTS = []

    WSGI_APPLICATION = "virtual_algerian_republic.wsgi.application"

    EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
    EMAIL_SERVICE_ENABLED = True
    EMAIL_HOST_USER = os.environ["EMAIL_HOST_USER"]
    EMAIL_RECIPIENT = os.environ["EMAIL_RECIPIENT"]


# Production mode configs include:
# 1- Debug mode,
# 2- Allowed hosts,
# 3- SMTP configs
# 4- REST framework config
elif os.environ["ENV_MODE"] == "PRODUCTION":

    DEBUG = False

    ALLOWED_HOSTS = []

    EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
    EMAIL_SERVICE_ENABLED = True
    EMAIL_HOST = os.environ["EMAIL_HOST"]
    EMAIL_PORT = os.environ["EMAIL_PORT"]
    EMAIL_HOST_USER = os.environ["EMAIL_HOST_USER"]
    EMAIL_HOST_PASSWORD = os.environ["EMAIL_HOST_PASSWORD"]
    EMAIL_RECIPIENT = os.environ["EMAIL_RECIPIENT"]
    EMAIL_USE_TLS = True  # or EMAIL_USE_SSL = TRUE & EMAIL_PORT = 465
    EMAIL_TIMEOUT = 30

    REST_FRAMEWORK.update({
        "DEFAULT_RENDERER_CLASSES": (
            "rest_framework.renderers.JSONRenderer",
        )
    })


# Application definition
INSTALLED_APPS = [

    # Built-in apps
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",

    # Third party apps
    "rest_framework",
    "corsheaders",

    # My own apps
    "virtual_algerian_republic.contact",
    "virtual_algerian_republic.content"

]


# Database
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": os.environ["DATABASE_NAME"],
        "USER": os.environ["DATABASE_USER"],
        "PASSWORD": os.environ["DATABASE_PASSWORD"],
        "HOST": os.environ["DATABASE_HOST"],
        "PORT": os.environ["DATABASE_PORT"],
        "TEST": {
            "NAME": os.environ["TEST_DATABASE_NAME"]
        },
    }
}


# SSR Templates
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            # Generic templates folder
            os.path.join(BASE_DIR, "templates"),
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]


# Middleware
MIDDLEWARE = [

    # Built-in middleware
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",

    # Third party middleware
    "corsheaders.middleware.CorsMiddleware",

    # Built-in middleware
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]


# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


# Internationalization
LANGUAGE_CODE = "en-us"
TIME_ZONE = os.environ["TIME_ZONE"]
USE_TZ = True
USE_I18N = False
USE_L10N = False
