# Dependencies
from django.contrib import admin
from django.urls import path, include
from django.conf import settings


# Application url patterns
urlpatterns = [

    # API URL(s)
    path(
        "api/",
        include(
            [
                path(
                    "content/",
                    include("virtual_algerian_republic.content.api.urls"),
                    name="content"
                ),
                path(
                    "contact/",
                    include("virtual_algerian_republic.contact.api.urls"),
                    name="contact"
                ),
            ]
        ),
        name="api-endpoints"
    ),

]


# Admin dashboard access
if settings.DEBUG == True:
    urlpatterns.append(
        # Built-in app URL(s)
        path(
            "admin/",
            admin.site.urls
        ),
    )
