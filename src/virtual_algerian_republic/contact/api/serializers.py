# Dependencies
from rest_framework import serializers
from ..models import (
    Subject,
    Message
)


# Subject serializer
class SubjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Subject
        fields = (  # "__all__"
            "label",
            "description",
            "created_at",
            "updated_at",
            "deleted_at"
            # "status"
        )


# Message serializer
class MessageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Message
        extra_kwargs = {
            "created_at": {
                "read_only": True
            },
            "updated_at": {
                "read_only": True
            },
            "deleted_at": {
                "read_only": True
            }
        }

        fields = (  # "__all__"
            "email",
            "subject",
            "content",
            "created_at",
            "updated_at",
            "deleted_at"
            # "response_status"
        )
