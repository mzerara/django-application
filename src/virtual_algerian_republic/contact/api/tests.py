# Dependencies
from django.conf import settings
from django.urls import reverse, resolve
from django.test import SimpleTestCase
from rest_framework import status
from rest_framework.test import APITestCase
from .views import (
    SubjectListView,
    MessageCreateView
)
from ..models import (
    Subject
)


# Globals
contact_url_configs = settings.PROJECT_CONFIGS["urls"]["contact_app"]


# Test suites
class ContactAPITestCase(APITestCase):

    def test_get_subject_list(self):

        reversed_url = reverse(
            contact_url_configs["list_of_subjects"]["route"]
        )
        response = self.client.get(
            reversed_url,
            format="json"
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK
        )

    def test_post_email_with_invalid_data(self):

        reversed_url = reverse(
            contact_url_configs["email"]["route"]
        )
        response = self.client.post(
            reversed_url,
            {
                "email": "test@test.com",
            },
            format="json"
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_400_BAD_REQUEST
        )

    def test_post_email_with_valid_data(self):

        # Mock Subject table
        Subject.objects.create(label="join_team")

        reversed_url = reverse(
            contact_url_configs["email"]["route"]
        )
        response = self.client.post(
            reversed_url,
            {
                "email": "test@test.com",
                "subject": "join_team",
                "content": "this is a test content",
            },
            format="json"
        )
        self.assertEqual(
            response.status_code,
            status.HTTP_201_CREATED
        )


class ContactUrlTestCase(SimpleTestCase):

    def test_subject_list_url_is_resolved(self):
        reversed_url = reverse(
            contact_url_configs["list_of_subjects"]["route"]
        )
        resolved_url = resolve(reversed_url)
        self.assertEqual(resolved_url.func.view_class, SubjectListView)

    def test_email_url_is_resolved(self):
        reversed_url = reverse(
            contact_url_configs["email"]["route"]
        )
        resolved_url = resolve(reversed_url)
        self.assertEqual(resolved_url.func.view_class, MessageCreateView)
