# Dependencies
from django.conf import settings
from django.urls import path
from .views import (
    SubjectListView,
    MessageCreateView
)


# Globals
contact_url_configs = settings.PROJECT_CONFIGS["urls"]["contact_app"]


# Register the application API urls
urlpatterns = [
    path(
        name=contact_url_configs["list_of_subjects"]["name"],
        route=contact_url_configs["list_of_subjects"]["route"],
        view=SubjectListView.as_view()
    ),
    path(
        name=contact_url_configs["email"]["name"],
        route=contact_url_configs["email"]["route"],
        view=MessageCreateView.as_view()
    )
]
