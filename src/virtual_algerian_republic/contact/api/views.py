# Dependencies
from django.conf import settings
from django.http import HttpResponse
from rest_framework.response import Response
from django.core.mail import (
    send_mail,
    BadHeaderError
)
from rest_framework import (
    generics,
    mixins,
    status,
)
from .serializers import (
    SubjectSerializer,
    MessageSerializer
)
from ..models import (
    Subject,
    Message
)


# Globals
contact_model_configs = settings.PROJECT_CONFIGS["models"]["contact_app"]
error_messages = settings.PROJECT_CONFIGS["error_messages"]


# Subject list API view
class SubjectListView(
    generics.GenericAPIView,
    mixins.ListModelMixin
):

    lookup_field = "pk"
    serializer_class = SubjectSerializer
    queryset = Subject.objects.all().filter(
        status=contact_model_configs["status"]["active"]
    ).order_by('created_at')

    def get(self, request):
        return self.list(request)


# Message list API view
class MessageCreateView(
    generics.GenericAPIView
):

    lookup_field = "pk"
    serializer_class = MessageSerializer

    def post(self, request):
        serializer = MessageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            if settings.EMAIL_SERVICE_ENABLED:

                try:

                    # Send an email to VAR group
                    send_mail(
                        subject="{0} - from:{1}".format(
                            request.data["subject"],
                            request.data["email"],
                        ),
                        message=request.data["content"],
                        from_email=settings.EMAIL_HOST_USER,
                        recipient_list=[
                            settings.EMAIL_RECIPIENT
                        ],
                        fail_silently=False,
                    )

                    # Send a receipt confirmation email to the sender
                    # ToDO later

                except BadHeaderError:
                    return Response(error_messages["invalid_header_found"], status=status.HTTP_400_BAD_REQUEST)

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
