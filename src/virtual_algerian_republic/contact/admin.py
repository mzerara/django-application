# Dependencies
from django.contrib import admin
from .models import(
    Subject,
    Message
)


# Register models here
admin.site.register(Subject)
admin.site.register(Message)
