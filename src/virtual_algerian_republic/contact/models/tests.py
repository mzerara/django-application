# Dependencies
from django.test import TestCase
from django.db import IntegrityError
from .models import (
    Subject,
    Message
)


# Test suites
class SubjectModelTest(TestCase):

    def test_subject_creation(self):

        subject = Subject.objects.create(
            label="seek_information",
            description="this is a test description"
        )

        self.assertEqual(Subject.objects.count(), 1)
        self.assertTrue(isinstance(subject, Subject))
        self.assertEqual(subject.__str__(), subject.label)


class MessageModelTest(TestCase):

    def test_message_creation_without_existing_subject(self):

        with self.assertRaises(IntegrityError):
            message = Message.objects.create(
                email="test@test.com",
                content="this is a simple test"
            )
            self.assertEqual(Message.objects.count(), 0)

    def test_message_creation_with_existing_subject(self):

        subject_label = "seek_information"

        Subject.objects.create(
            label=subject_label,
            description="this is a test description"
        )

        message = Message.objects.create(
            email="test@test.com",
            subject=Subject.objects.get(label=subject_label),
            content="this is a simple test"
        )

        self.assertEqual(Message.objects.count(), 1)
        self.assertTrue(isinstance(message, Message))
        self.assertEqual(message.response_status, "pending")
        self.assertEqual(
            message.__str__(),
            "test@test.com -> #seek_information"
        )
