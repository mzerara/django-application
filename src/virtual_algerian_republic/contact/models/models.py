# Dependencies
from django.db import models
from django.conf import settings


# Globals
contact_model_configs = settings.PROJECT_CONFIGS["models"]["contact_app"]


# Subject table
class Subject(models.Model):

    label = models.CharField(
        primary_key=True,
        max_length=50
    )
    description = models.TextField(
        blank=True
    )
    created_at = models.DateTimeField(
        auto_now=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        auto_now_add=False
    )
    deleted_at = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True
    )
    status = models.CharField(
        max_length=10,
        default=contact_model_configs["status"]["active"],
        choices=[
            (key, value.capitalize()) for (key, value) in contact_model_configs["status"].items()
        ]
    )

    def __str__(self):
        return self.label


# Message table
class Message(models.Model):

    email = models.EmailField(
        blank=False,
        null=False
    )
    subject = models.ForeignKey(
        to="Subject",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
    )
    content = models.TextField(
        blank=False,
        null=False
    )
    created_at = models.DateTimeField(
        auto_now=False,
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        auto_now_add=False
    )
    deleted_at = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True
    )
    response_status = models.CharField(
        max_length=10,
        default=contact_model_configs["response_status"]["pending"],
        choices=[
            (key, value.capitalize()) for (key, value) in contact_model_configs["response_status"].items()
        ]
    )

    def __str__(self):
        return "{0} -> #{1}".format(
            self.email,
            self.subject
        )
