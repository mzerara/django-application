# Dependencies
from django.core.management.base import BaseCommand
from django.conf import settings
from ...models import (
    Subject,
)


# Globals
contact_model_configs = settings.PROJECT_CONFIGS["models"]["contact_app"]


# Create your custom actions here.
class Command(BaseCommand):

    help = "This command will insert some initial content data in the database"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):

        # Ceate initial media elements
        self.stdout.write(
            "***** Inserting data into Subject table *****",
            ending='\n'
        )
        for subject in contact_model_configs["subjects"].keys():
            Subject.objects.update_or_create(
                label=subject,
                description=contact_model_configs["subjects"][subject]
            )
