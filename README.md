### This is a manually configured project. 

# Instructions

### Setup project 

1. Install docker and docker-compose 

2. Clone project  
```code
git clone https://mzerara@bitbucket.org/mzerara/var-backend.git 
```
3. Read the *.env_example* file to set all the required environment varialbes in a *.env* file 

4. Create and start docker containers
```code
cd var-backend
docker-compose up 
```
5. Install project dependencies, apply database migrations and seeding 
```code
docker exec -it var_backend bash
pipenv shell
pipenv install --dev  
cd src 
python manage.py migrate
python manage.py content_seed
python manage.py contact_seed
```
6. Detach from docker container  

### Launch project 

1. Run docker containers
```code
cd var-backend
docker-compose up 
```
